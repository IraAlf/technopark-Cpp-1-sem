#include<stdio.h>
#include<stdlib.h>
#include <string.h>

#include "ioarray.h"
#include "errors.h"
#include "alignment.h"

int main()
{
    int n = 0;
    int max_len = 0;
    int rc = scanf_of_n(&n);
    if (rc == OK)
    {
        struct element **a = scanf_of_vectors(n, &max_len);
        if (a) {
            a = alignment(a, n, max_len);
            if (a)
                printf_of_arrays(a, n, max_len);
            else
            {
                rc = ALI_ERROR;
                print_of_mistake(ALI_ERROR);
            }
        }
        else
        {
            rc = MEM_ERROR;
            print_of_mistake(MEM_ERROR);
        }
    }
    else
    {
        rc = VECT_ERROR;
        print_of_mistake(VECT_ERROR);
    }
    return rc;
}