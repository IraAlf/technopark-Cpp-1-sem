//
// Created by irinina on 11.10.2020.
//

#ifndef UNTITLED1_ERRORS_H
#define UNTITLED1_ERRORS_H

#define OK 0
#define DATA_ERROR -1
#define MEM_ERROR -2
#define VECT_ERROR -3
#define ALI_ERROR -4

#endif //UNTITLED1_ERRORS_H
