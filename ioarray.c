//
// Created by irinina on 11.10.2020.
//

#include <stdio.h>
#include <stdlib.h>
#include "ioarray.h"
#include "errors.h"
#include "alignment.h"

struct element* scanf_of_vector(int *len);

int scanf_of_n(int *n)
{
    printf("Input count of vectors: ");
    int rc = scanf("%d", n);
    if (rc == 1)
        rc = OK;
    else
        rc = DATA_ERROR;
    return rc;
}

struct element* scanf_of_vector(int *len)
{
    printf("Input len of current vector: ");
    int n;
    int rc = scanf("%d", &n);
    *len = n;
    struct element *array = NULL;
    if (rc == 1)
    {
        array = (struct element*)malloc((n) * sizeof (struct element));
        for (int i = 0; i < n && rc == 1; i++)
        {
            printf("Input element of vector: ");
            rc = scanf("%d", &array[i].value);
            if (rc == 1)
                array[i].ptr = i == n - 1? NULL: array + i;
        }
        if (rc != 1)
            rc = DATA_ERROR;
    }
    return array;
}

struct element **scanf_of_vectors(int n, int *maxlen)
{
    int len = 0;
    *maxlen = 0;
    struct element **arr = (struct element**)malloc(n * sizeof (struct element*));
    for(int i = 0; i < n; i++) {
        arr[i] = scanf_of_vector(&len);
        if (len > *maxlen)
            *maxlen = len;
        if (!arr[i]) {
            for (int j = 0; j < i; j++)
                free(arr[j]);
            free(arr);
            arr = NULL;
            break;
        }
    }
    return arr;
}

void printf_of_arrays(struct element **a, int len, int max_len)
{
    for (int i = 0; i < len; i++)
    {
        for (int j = 0; j < max_len; j++)
            printf("%d ", a[i][j].value);
        printf("\n");
    }
}


void print_of_mistake(int kind)
{
    switch (kind) {
        case MEM_ERROR:
        {
            printf("Memory mistake!");
            break;
        }
        case ALI_ERROR:
        {
            printf("Mistake of alignment!");
            break;
        }
        case VECT_ERROR:
        {
            printf("Mistake! Incorrect count of vectors!");
            break;
        }
        default:
            break;
    }
}